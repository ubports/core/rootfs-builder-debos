#!/bin/sh -eux

# If /etc/resolv.conf is not managed by either systemd-resolved or resolvconf,
# we want NetworkManager to manage it. But it can't do so if /etc is read-only.
# Thus, ahead-of-time delegate this responsibility using a symlink.

if ! type resolvectl >/dev/null && ! type resolvconf >/dev/null; then
    rm -v /etc/resolv.conf
    ln -svf /run/NetworkManager/resolv.conf /etc/resolv.conf
fi

# If systemd-resolved is not installed, then divert the config to tell NM to use
# it. This worksaround https://bugs.launchpad.net/bugs/2097792
if [ "$(dpkg-query --show --showformat='${db:Status-Status}\n' systemd-resolved 2>/dev/null)" != "installed" ]; then
    dpkg-divert --add --local --rename \
                --divert /usr/lib/NetworkManager/conf.d/10-dns-resolved.conf.disabled-by-rootfs-builder-debos \
                /usr/lib/NetworkManager/conf.d/10-dns-resolved.conf
fi
